<?php

class MY_Session extends CI_Session
{

    // ------------------------------------------------------------------------

    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    // ------------------------------------------------------------------------

    public function is_logged()
    {
        if (!$this->userdata('user_id'))
        {
            $this->sess_destroy();
            header('location: ' . site_url('/'));
        }
    }

    // ------------------------------------------------------------------------

    /**
     * Verify the user is a certain type
     *
     * @param string|array $type
     */
    public function is_type($type)
    {
        $redirect = false;

        if (is_array($type)) {
            if (!in_array($this->userdata('type'), $type)) {
                $redirect = true;
            }
        }
        else
        {
            if ($this->userdata('type') != $type)
            {
                $redirect = true;
            }
        }

        if ($redirect === true) {
            header('location: ' . site_url('/'));
        }
    }

    // ------------------------------------------------------------------------

}