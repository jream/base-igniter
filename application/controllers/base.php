<?php

class Base extends CI_Controller {

    protected $data = array();

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $S3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);

        $this->data['head'] = $this->load->view('shared/inc/head_view', null, true);
        $this->data['analytics'] = $this->load->view('shared/inc/analytics_view', null, true);
    }

    // ------------------------------------------------------------------------

    /**
     * output - Prepare JSON encoded data in a uniform package
     *
     * @param integer|array $success Result of 1 or 0, or combine
     *                       the two parameters in one, eg: [0, 'Message', 'Message2']
     * @param array $data Pass back data to response
     */
    public function output($success, $data = array())
    {
        if (is_array($success))
        {
            if (!is_numeric($success[0])) {
                throw new Exception('Your first value must be numeric');
            }

            $output = array('result' => $success[0], array_shift($success));
        }
        else
        {
            if ($success == 1)
            $output = array('result' => 1, 'data' => $data);

            else
            $output = array('result' => 0, 'error' => $data);
        }

        header('Content-Type: application/json');
        echo json_encode($output);
        exit;
    }

    // ------------------------------------------------------------------------

    /**
     * slugify - Creates a slugs and checks against the database
     *
     * @param string $string Likely a title
     * @param string $table
     */
    public function slugify($string, $table)
    {
        $slug = url_title($string);
        $slug = strtolower($slug);
        $i = 0;

        $params = array();
        $params['slug'] = $slug;

        while ($this->db->where($params)->get($table)->num_rows())
        {
            if (!preg_match ('/-{1}[0-9]+$/', $slug )) {
                $slug .= '-' . ++$i;
            } else {
                $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
            }
            $params['slug'] = $slug;
        }

        return $slug;
    }

    // ------------------------------------------------------------------------
}