<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Util extends Base {

    // ------------------------------------------------------------------------

    public $data = array();

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
    }

    // ------------------------------------------------------------------------

    public function index()
    {
        echo "Test Area";
    }

    // ------------------------------------------------------------------------

    public function password($password = false)
    {
        if (!$password) {
            echo "Please provide a string in the URI";
            return false;
        }

        echo hash('sha256', $password . $this->config->item('encryption_key'));
    }

    // ------------------------------------------------------------------------

    public function uuid()
    {
        // $this->load->model('user_model', 'user');
        // $result = $this->user->get();
        // foreach($result as $_key => $_value) {
        //     echo $_value['user_id'];
        //     $this->user->update(array(
        //         'uuid' => sprintf('%s%s',
        //             uniqid(),
        //             substr(hash('sha256', uniqid()), 4, 12)
        //         )
        //     ), $_value['user_id']);
        // }
    }
    // ------------------------------------------------------------------------


}
