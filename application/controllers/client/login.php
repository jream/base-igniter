<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Base {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('user_id')) {
            $type = $this->session->userdata('type');
            redirect('client/dashboard');
        }
    }

    // ------------------------------------------------------------------------

    /**
    * Loads the re-usable views for this controller
    * Should be called AFTER Settings $this->data['title'] or any others
    * to be parsed into those views.
    */
    private function _load_assets()
    {
        $this->data['header'] = $this->load->view('shared/inc/header_view', $this->data, true);
        $this->data['footer'] = $this->load->view('shared/inc/footer_view', $this->data, true);
    }

    // ------------------------------------------------------------------------

    public function index()
    {
        $this->data['title'] = 'Login';
        $this->data['user_type'] = 'client';
        $this->_load_assets();
        $this->load->view('shared/login_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function register()
    {
        $this->data['title'] = 'Register';
        $this->_load_assets();
        $this->load->view('shared/register_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function forgot_password()
    {
        $this->data['title'] = 'Forgot Password';
        $this->_load_assets();
        $this->load->view('shared/forgot_password_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function change_password($user_id = false, $key = false)
    {
        $this->data['key'] = trim($key);
        if ($key == false || strlen($key) < 64) {
            $this->data['has_valid_key'] = false;
        } else {
            $this->db->select('key');
            $this->db->where('key', $key);
            $this->db->where('user_id', $user_id);
            $query = $this->db->get('user');
            $result = $query->row_array();

            $this->data['has_valid_key'] = true;
            if (empty($result)) {
                $this->data['has_valid_key'] = false;
            }
        }

        $this->data['user_id'] = $user_id;
        $this->data['title'] = 'Change Password';
        $this->_load_assets();
        $this->load->view('shared/change_password_view', $this->data);
    }

    // ------------------------------------------------------------------------

}
