<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends Base {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('user_id')) {
            redirect('client/login');
        }
    }

    // ------------------------------------------------------------------------

    /**
    * Loads the re-usable views for this controller
    * Should be called AFTER Settings $this->data['title'] or any others
    * to be parsed into those views.
    */
    private function _load_assets()
    {
        $this->data['header'] = $this->load->view('client/inc/header_view', $this->data, true);
        $this->data['footer'] = $this->load->view('client/inc/footer_view', $this->data, true);
    }

    // ------------------------------------------------------------------------

    public function index()
    {
        $this->redirect('client');
    }

    // ------------------------------------------------------------------------

    public function profile()
    {
        $user_id = $this->session->userdata('user_id');

        $this->data['title'] = 'Profile';
        $this->_load_assets();
        $this->data['user'] = $this->user->meta->get_bundle($user_id);
        $this->load->view('client/profile_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function project()
    {
        $user_id = $this->session->userdata('user_id');
        $this->load->model('project_model', 'project');
        $this->data['project'] = $this->project->get(array('user_id' => $user_id));

        $this->data['title'] = 'Project';
        $this->_load_assets();
        $this->load->view('client/project_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function content()
    {
        $user_id = $this->session->userdata('user_id');

        $this->data['title'] = 'Content';
        $this->_load_assets();
        $this->load->view('client/content_view', $this->data);
    }

    // ------------------------------------------------------------------------

}
