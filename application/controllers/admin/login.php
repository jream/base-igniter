<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Base {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();

        $this->data['title'] = 'Admin Login';
        $this->data['header'] = $this->load->view('shared/inc/header_view', $this->data, true);
        $this->data['footer'] = $this->load->view('shared/inc/footer_view', $this->data, true);
    }

    // ------------------------------------------------------------------------

    public function index()
    {
        if ($this->session->userdata('user_id')) {
            $type = $this->session->userdata('type');
            redirect(site_url('admin/dashboard'));
        }

        $this->data['user_type'] = 'admin';
        $this->load->view('shared/login_view', $this->data);
    }

    // ------------------------------------------------------------------------

}
