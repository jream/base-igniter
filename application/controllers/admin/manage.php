<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Manage extends Base {

    // ------------------------------------------------------------------------

    public function __construct()
    {
        parent::__construct();
        $this->session->is_type('admin');
    }

    // ------------------------------------------------------------------------

    /**
    * Loads the re-usable views for this controller
    * Should be called AFTER Settings $this->data['title'] or any others
    * to be parsed into those views.
    */
    private function _load_assets()
    {
        $this->data['header'] = $this->load->view('admin/inc/header_view', $this->data, true);
        $this->data['footer'] = $this->load->view('admin/inc/footer_view', $this->data, true);
    }

    // ------------------------------------------------------------------------

    public function index()
    {
        $this->redirect('admin');
    }

    // ------------------------------------------------------------------------

    public function profile()
    {
        $user_id = $this->session->userdata('user_id');
        $this->data['user'] = $this->user->meta->get_bundle($user_id);

        $this->data['title'] = 'Admin Profile';
        $this->_load_assets();
        $this->load->view('admin/profile_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function user()
    {
        $this->data['client'] = $this->user->meta->get_bundle();
        $this->data['title'] = 'Users';
        $this->_load_assets();
        $this->load->view('admin/manage_user_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function category()
    {
        $this->data['category'] = $this->category->get();
        $this->data['title'] = 'Categories';
        $this->_load_assets();
        $this->load->view('admin/manage_category_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function account()
    {
        $this->data['account'] = $this->user->get($this->user_id);
        $this->data['title'] = 'Account';
        $this->_load_assets();
        $this->load->view('admin/account_view', $this->data);
    }

    // ------------------------------------------------------------------------

    public function do_delete()
    {
        $user_id = $this->input->post('user_id');

        $delete_from = array(
            'user',
            'user_meta',
        );

        foreach ($delete_from as $_key => $_value)
        {
            $this->db->where('user_id', $user_id);
            $this->db->delete($_value);
        }

        $this->output(1);
    }

    // ------------------------------------------------------------------------

    public function do_ban()
    {
        $user_id = $this->input->post('user_id');
        $bool = $this->input->post('bool');

        $this->db->where('type !=', 'admin');
        $this->db->where('user_id !=', $user_id);
        $result = $this->user->update(array(
            'banned' => $bool,
        ), $user_id);

        if ($result) {
            $this->output(1);
        }
        $this->output(0);
    }

    // ------------------------------------------------------------------------

    public function do_activate()
    {
        $user_id = $this->input->post('user_id');
        $bool = $this->input->post('bool');

        $result = $this->user->update(array(
            'activated' => $bool
        ), $user_id);

        if ($result) {
            $this->output(1);
        }

        $this->output(0);
    }

    // ------------------------------------------------------------------------

    public function do_create_user()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|matches[confirm_password]');

        if ($this->form_validation->run() == false) {
            $this->form_validation->error_array();
            foreach($error as $_key => $_value) {
                $this->session->set_flashdata($_key, $_value);
            }
            redirect('admin/account_setting');
            exit;
        }

        $email = $this->input->post('email');

        $result = $this->db->insert('user', array(
            'email' => $email,
            'password' => hash('sha256', $this->input->post('password') . $this->config->item('encryption_key'))
        ));

    }

    // ------------------------------------------------------------------------

    public function do_account_setting()
    {
        $this->form_validation->set_rules('email', 'Email', 'required|email');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|matches[confirm_password]');

        if ($this->form_validation->run() == false) {
            $this->form_validation->error_array();
            foreach($error as $_key => $_value) {
                $this->session->set_flashdata($_key, $_value);
            }
            redirect('admin/account_setting');
            exit;
        }

        $email = $this->input->post('email');

        $this->db->where('user_id', $this->input->post('user_id'));
        $this->db->update('user', array(
            'email' => $email,
            'password' => hash('sha256', $this->input->post('password') . $this->config->item('encryption_key'))
        ));

    }

    // ------------------------------------------------------------------------

}
