<?=$header?>

<div class="title">
    <h3>Create Your Account</h3>
</div>

<div class="span4">

    <form id="register_form" action="<?=site_url('api_user/do_register')?>" method="post" class="form-horizontal">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
        <div class="control-group">
            <label class="control-label">Email</label>
            <div class="controls">
                <input type="text" name="email" value="" placeholder="Email" class="login" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                <input type="password" name="password" value="" placeholder="Password" class="login"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Confirm Password</label>
            <div class="controls">
                <input type="password" name="confirm_password" value="" placeholder="Confirm Password" class="login"/>
            </div>
        </div>

        <div class="control-group">
            <div class="ajax-loader hide pull-right text-center">
                Please Wait<br />
                <img class="pull-right" src="<?=site_url('public/img/ajax-loader.gif')?>" alt="Loading" />
            </div>
            <div class="controls">
                <button class="btn-register button btn btn-primary btn-large">Register</button>
            </div>
        </div>

    </form>


    <!-- Text Under Box -->
    <div class="login-extra">
    	Already have an account? <a href="<?=site_url('client/login')?>">Login</a>
    </div> <!-- /login-extra -->

</div>

<div class="clearfix"></div>

<script>
$(function() {

    $("#register_form").submit(function(e) {
        e.preventDefault();

        $(".btn-register").addClass('hide');
        $(".ajax-loader").removeClass('hide');

        var url = $(this).attr('action');
        var postData = $(this).serialize();

        $.post(url, postData, function(o) {
            if (o.result == 1) {
                Result.success('Your account has been created.');
                window.location.href = '<?=site_url()?>' + o.data.redirect;
            } else {
                $(".btn-register").removeClass('hide');
                $(".ajax-loader").addClass('hide');
                Result.error(o.error);
            }
        }, 'json')

    })
});
</script>


<?=$footer?>