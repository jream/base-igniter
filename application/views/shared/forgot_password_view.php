<?=$header?>

<div class="title">
    <h3>Forgot Password</h3>
</div>

<div class="span4">

    <div class="account-container register stacked">
        <div class="content clearfix">

            <form id="forgot_password_form" class="form-horizontal" action="<?=site_url('api_user/do_forgot_password')?>" method="post">
                <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
                <div class="control-group">
                    <label class="control-label">Email</label>
                    <div class="controls">
                        <input type="text" id="login" name="email" value="" placeholder="Email" class="login username-field" />
                    </div>
                </div>

                <div class="control-group">
                    <div class="ajax-loader hide pull-right text-center">
                        Please Wait<br />
                        <img class="pull-right" src="<?=site_url('public/img/ajax-loader.gif')?>" alt="Loading" />
                    </div>
                    <div class="controls">
                        <input type="submit" class="button btn btn-primary" value="Submit" />
                    </div>
                </div>

            </form>

        </div> <!-- /content -->

    </div> <!-- /account-container -->

    <!-- Text Under Box -->
    <div class="login-extra">
        Already have an account? <a href="<?=site_url('client/login')?>">Login</a>
    </div> <!-- /login-extra -->

</div>

<script>
$(function() {
    $("#forgot_password_form").submit(function(e) {
        e.preventDefault();

        var url = $(this).attr('action');
        var postData = $(this).serialize();

        $(".ajax-loader").removeClass('hide');
        $(".submit-btn").addClass('hide');

        $.post(url, postData, function(o) {
            if (o.result == 1) {
                $(".content").html('<h1>Complete!</h1><p>Please check your email for further instructions.</p>')
                Result.success('Please check your email for password reset instructions.');
            } else {
                Result.error(o.error);
                $(".ajax-loader").addClass('hide');
                $(".submit-btn").removeClass('hide');
            }
        }, 'json')

    })
});
</script>

<?=$footer?>