<link href="<?=site_url('public/third-party/css/bootstrap.css')?>" rel="stylesheet">
<link href="<?=site_url('public/third-party/css/bootstrap-fileupload.min.css')?>" rel="stylesheet">
<link href="<?=site_url('public/css/shared.css')?>" rel="stylesheet">
<script src="<?=site_url('public/third-party/js/jquery.min.js')?>"></script>
<script src="<?=site_url('public/third-party/js/bootstrap.min.js')?>"></script>
<script src="<?=site_url('public/third-party/js/bootstrap-fileupload.min.js')?>"></script>
<script src="<?=site_url('public/third-party/js/jquery.form.js')?>"></script>
<script src="<?=site_url('public/js/ajax.js')?>"></script>
<script src="<?=site_url('public/js/result.js')?>"></script>

<script>
$(function() {
    Result = new Result();
});
</script>
