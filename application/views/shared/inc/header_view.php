<!doctype html>
<html>
<head>
    <title><?=@$title?></title>
    <?=$head?>
</head>
<body>

<div id="shared-header">
    <div class="container">
        <h1><a href="<?=site_url('index')?>">Base Igniter</a></h1>
    </div>
</div>

<div id="wrap" class="container">

<ul class="nav  nav-tabs">
    <?php $seg = $this->uri->segment(1);?>
    <li class="<?=($seg == '' || $seg == 'index') ? 'active' : ''?>"><a href="<?=site_url('/')?>">Home</a></li>
    <li class="<?=($seg == 'user') ? 'active' : ''?>"><a href="<?=site_url('user')?>">Users</a></li>
    <li class="<?=($seg == 'admin') ? 'active' : ''?>"><a href="<?=site_url('admin')?>">Admin</a></li>
    <li class="<?=($seg == 'client') ? 'active' : ''?>"><a href="<?=site_url('client')?>">Login</a></li>
</ul>

  <div id="success" class="alert alert-success"></div>
  <div id="error" class="alert alert-error"></div>
