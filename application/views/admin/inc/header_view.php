<!doctype html>
<html>
<head>
    <title><?=@$title?></title>
    <?=$head?>
</head>
<body>

<div id="header">
    <div class="container">
        <h3 class="muted"><?=@$title?></h3>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">
                    <ul class="nav">
                        <?php $seg = $this->uri->segment(3);?>
                        <li class="<?=($seg == '' || $seg == 'dashboard') ? 'active' : ''?>"><a href="<?=site_url('admin/dashboard')?>">Dashboard</a></li>
                        <li class="<?=($seg == 'profile') ? 'active' : ''?>"><a href="<?=site_url('admin/manage/profile')?>">Profile</a></li>
                        <li class="<?=($seg == 'user') ? 'active' : ''?>"><a href="<?=site_url('admin/manage/user')?>">Users</a></li>
                        <li><a href="<?=site_url('api_user/do_logout')?>">Logout</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div><!-- /.navbar -->
</div>

<div id="wrap" class="container">

    <div id="success" class="alert alert-success"></div>
    <div id="error" class="alert alert-error"></div>
