<?=$header;?>

<div class="span5">

    <h4>Update Profile</h4>
    <form id="profile-form" action="<?=site_url('api_user/do_profile')?>">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
        <div class="control-group">
            <label class="control-label">Name</label>
            <div class="controls">
                <input type="text" name="first_name" class="input-medium" value="<?=@$user['profile']['first_name']?>">
                <input type="text" name="last_name" class="input-medium" value="<?=@$user['profile']['last_name']?>">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">Email</label>
            <div class="controls">
                <input type="text" name="email" value="<?=@$user['email']?>">
            </div>
            <small>* A new email will change your login</small>
        </div>

        <div class="control-group">
            <div class="controls">
                <input type="submit" value="Update" class="btn btn-primary" />
            </div>
        </div>
    </form>

</div>
<div class="span5">
    <h4>Avatar</h4>
    <form id="avatar-form" action="<?=site_url('api_user/do_avatar')?>" method="post" enctype="multipart/form-data">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
        <?php if (isset($user['profile']['avatar'])):?>
        <div id="avatar">
            <img src="<?=site_url(UPLOAD_PATH . $user['profile']['avatar_th'])?>" alt="Avatar" />
            <a id="do_avatar_rm" href="<?=site_url('api_user/do_avatar_rm')?>">Remove</a>
        </div>
        <?php endif;?>

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;"></div>
                <div>
                <span class="btn btn-file">
                    <span class="fileupload-new">Select image</span>
                    <span class="fileupload-exists">Change</span>
                    <input type="file" name="avatar" />
                </span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
        </div>

        <div class="progress progress-striped active">
            <div class="bar"></div>
        </div>

        <div class="control-group">
            <div class="controls">
                <input type="submit" value="Upload" class="btn btn-primary" />
            </div>
        </div>

    </form>
</div>

<div class="clearfix"></div>

<div class="span5">
    <h4>Change Password</h4>
    <form id="password-form" action="<?=site_url('api_user/do_change_password')?>">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
        <div class="control-group">
            <label class="control-label">New Password</label>
            <div class="controls">
                <input type="text" name="password" class="input-medium">
                <input type="text" name="confirm_password" class="input-medium">
            </div>
        </div>

        <input type="hidden" name="user_id" value="<?=@$user['user_id']?>" />
        <input type="hidden" name="key" value="<?=@$user['key']?>" />

        <div class="control-group">
            <div class="controls">
                <input type="submit" value="Update" class="btn btn-primary" />
            </div>
        </div>

    </form>

</div>

<div class="clearfix"></div>

<script>
$(function() {

    handle_form('#profile-form', 'Profile successfully updated.');
    handle_form('#password-form', 'Password successfully updated.');

    var bar = $('.bar');
    var percent = $('.percent');

    $("#avatar-form").ajaxForm({
        dataType: 'json',
        beforeSend: function() {
            var percentVal = '0%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            var percentVal = percentComplete + '%';
            bar.width(percentVal)
            percent.html(percentVal);
        },
        success: function(o) {
            if (o.result == 1) {
                var percentVal = '100%';
                bar.width(percentVal)
                percent.html(percentVal);
                Result.success('You\'ve Successfully uploaded your avatar.');
            } else {
                Result.error(o.error);
            }
        }
    });


    $("#do_avatar_rm").click(function(e) {
        e.preventDefault();
        var c = confirm("Are you sure?");
        if (c == false) return false;

        var url = $(this).attr('href');
        $.get(url, function(o) {
            if (o.result == 1) {
                $("#avatar").html('');
                Result.success('Avatar removed');
            } else {
                Result.error(o.error);
            }
        }, 'json');
    })

});
</script>

<?=$footer;?>