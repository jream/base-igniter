<?=$header;?>

    <?php if ($user['alias']):?>
        <h4>Dashboard</h4>
        <ul>
            <li>Your alias is: <?=$user['alias']?></li>
            <li>Your public url is <code><?=site_url("user/{$user['alias']}")?></code></li>
        </ul>

    <?php else:?>
        <h4>Set Your Alias</h4>
        <form id="alias-form" action="<?=site_url('api_user/do_alias')?>">
        <input type="hidden" name="<?=$this->security->get_csrf_token_name()?>" value="<?=$this->security->get_csrf_hash()?>" />
        <p>
            <strong>You may only set your alias once.</strong>
        </p>
        <p>
            This will be a permanent nickname for you to go by.
        </p>

            <div class="controls input-append">
                <input type="text" name="alias" />
                <input type="submit" value="Save" class="btn btn-primary" />
            </div>

    </form>
    <?php endif;?>


<script>
$(function() {

    $("#alias-form").submit(function(e) {
        e.preventDefault();

        var c = confirm('Are you sure this is the name you want? This is permanent.');
        if (c == false) return false;

        var url = $(this).attr('action');
        var postData = $(this).serialize();
        $.post(url, postData, function(o) {
            if (o.result) {
                Result.success('Saved');
                setTimeout(function() {
                    window.location.href = '?'
                }, 1000);
            } else {
                Result.error(o.error);
            }
        }, 'json');
    });

});
</script>
<?=$footer;?>