<?php

/*
|--------------------------------------------------------------------------
| Private Config
|--------------------------------------------------------------------------
*/
if (file_exists('private_config.php')) {
    require 'private_config.php';
}

/*
|--------------------------------------------------------------------------
| Constants
|--------------------------------------------------------------------------
*/
$constants = array(
    'EMAIL_FROM' => 'none@none.com',
    'EMAIL_FROM_NAME' => 'none',
    'AWS_BUCKET_NAME' => '',
    'AWS_ACCESS_KEY' => '',
    'AWS_SECRET_KEY' => ''
);

foreach ($constants as $_key => $_value) {
    if (!defined($_key))
    define($_key, $_value);
}

/*
| -------------------------------------------------------------------
|  Native Auto-load
| -------------------------------------------------------------------
|
| Nothing to do with config/autoload.php, this allows PHP autoload to work
| for base controllers and some third-party libraries.
|
*/
function __autoload($class)
{
    $class = strtolower($class);

    if ($class == 'base')
    require_once APPPATH . 'controllers/'. $class . EXT;

    if ($class == 'crud_model')
    require_once APPPATH . 'models/'. $class . EXT;

    if ($class == 's3')
    require APPPATH . 'third_party/S3/' . 'S3' . EXT;
}

/*
|--------------------------------------------------------------------------
| Time
|--------------------------------------------------------------------------
*/
date_default_timezone_set('UTC');
define('DATEFORMAT', 'F dS, Y');

// Do NOT change this it's for MySQL
define('DATETIME', date('Y-m-d H:i:s', time()));

/*
|--------------------------------------------------------------------------
| File Path
|--------------------------------------------------------------------------
|
| The Location for the file path set in one area
|
*/
if (!defined('UPLOAD_PATH')) {
    define('UPLOAD_PATH', realpath('upload/'));
}

