# Base-Igniter
---
The clean slate Code Igniter with necessary functionality to quickly launch projects without rebuilding things.
No core files modified.

- Author: Jesse Boyer
- Website: http://jream.com
- Version: 1.2a Dev

## Default Users
- Default Admin: admin@admin.com/a
- Default Client: c@c.com/a
- See configuration below!

## Built in Features
- Admin with User Management
- Login
- Registation
- Forgot Password
- Change Password
- Login Failure Timeouts

## System Setup
- Sub-folder organized Controllers
- Common Shared Views
- Custom Meta Model
- Custom CRUD Model

## Frontend Setup
- Prepared with jQuery
- Prepared with Twitter Bootstrap

# Configuration
Import the `schema.sql`

Make sure your `.htaccess` has the correct RewriteBase if you need it. `RewriteBase /base-igniter`. Be sure to remove the # comment!
There is a utility class

###Changing Encryption Key
The default encryption key is `jream.` You can change this in `application/config.php` and change `$config['encryption_key']` to whatever you like.

###Changing Passwords
You'll then want to generate new passwords for your users by visiting: `http://localhost/util/password/your_password_here` and populating your user password table with the new hash.





--

(C) 2013 Jesse Boyer